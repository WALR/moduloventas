--
-- Base de datos: `nombrebasedatos`
-- 
--

CREATE DATABASE IF NOT EXISTS `ModuloVentasDB`;

USE `ModuloVentasDB`;

CREATE USER 'ModuloVentasDBUser'@'%' IDENTIFIED BY 'ModuloVentasDBpass**';
GRANT ALL PRIVILEGES ON ModuloVentasDB.* TO 'ModuloVentasDBUser'@'%';
FLUSH PRIVILEGES;

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellido` varchar(100) DEFAULT NULL,
  `numero_empleado` int(11) DEFAULT NULL,
  `rol` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL
);

ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `username` (`username`);


ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT;


CREATE TABLE `rol` (
  `id_rol` int(11) NOT NULL,
  `codigo` varchar(40) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL
);

ALTER TABLE `rol`
  ADD PRIMARY KEY (`id_rol`),
  ADD UNIQUE KEY `codigo` (`codigo`);


ALTER TABLE `rol`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT;



INSERT INTO rol 
(codigo, nombre) 
VALUES 
('gycurso01', 'Admin');

INSERT INTO usuario 
(username, password, nombre, apellido, numero_empleado, rol, estado) 
VALUES 
('wilfredlemus', '$2y$10$JDZxhWO6yXSvBCp.9FnsUObBRKj46AUSOa9W374L0qPv4YQlWPRu6',
 'Wilfred', 'Lemus', 1234, 1, 1);

--  password_hash("1234pass", PASSWORD_DEFAULT)


--
-- Estructura de tabla para la tabla `producto`
--
CREATE TABLE `producto` (
  `id_producto` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `descripcion` varchar(400) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL
);
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`);
ALTER TABLE `producto`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT;


CREATE TABLE `subproducto` (
  `id_subproducto` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `descripcion` varchar(400) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL,
  `id_producto` int(11) NOT NULL
);
ALTER TABLE `subproducto`
  ADD PRIMARY KEY (`id_subproducto`);
ALTER TABLE `subproducto`
  MODIFY `id_subproducto` int(11) NOT NULL AUTO_INCREMENT;


CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL,
  `codigo_asociado` int(11) NULL,
  `primer_nombre` varchar(150) NOT NULL,
  `segundo_nombre` varchar(150) DEFAULT NULL,
  `primer_apellido` varchar(150) NOT NULL,
  `segundo_apellido` varchar(150) DEFAULT NULL,
  `apellido_casada` varchar(150) DEFAULT NULL,
  `fecha_nacimiento` date NULL,
  `dpi` bigint(20) DEFAULT NULL,
  `nit` int(11) DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `celular` int(11) DEFAULT NULL,
  `direccion` varchar(400) DEFAULT NULL,
  `asociado` tinyint(1) NOT NULL,
  `estado` tinyint(1) NOT NULL
);
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`);
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT;


CREATE TABLE `interes_cliente` (
  `id_interes_cliente` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `fecha_adquirir` datetime NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `notas` varchar(400) DEFAULT NULL,
  `estado` int(11) NOT NULL COMMENT '1=Activo, 2=Trabajado, 3=Inactivo'
);
ALTER TABLE `interes_cliente`
  ADD PRIMARY KEY (`id_interes_cliente`);
ALTER TABLE `interes_cliente`
  MODIFY `id_interes_cliente` int(11) NOT NULL AUTO_INCREMENT;


CREATE TABLE `interes_detalle` (
  `id_interes_detalle` int(11) NOT NULL,
  `interes_cliente` int(11) NOT NULL,
  `id_subproducto` int(11) NOT NULL
);
ALTER TABLE `interes_detalle`
  ADD PRIMARY KEY (`id_interes_detalle`);
ALTER TABLE `interes_detalle`
  MODIFY `id_interes_detalle` int(11) NOT NULL AUTO_INCREMENT;


--
-- Estructura de tabla para la tabla `cliente`
--
-- CREATE TABLE `cliente` (
--   `id_cliente` int(11) NOT NULL,
--   `primer_nombre` varchar(150) NOT NULL,
--   `segundo_nombre` varchar(150) DEFAULT NULL,
--   `primer_apellido` varchar(150) NOT NULL,
--   `segundo_apellido` varchar(150) DEFAULT NULL,
--   `fecha_nacimiento` date NOT NULL,
--   `dpi` bigint(20) DEFAULT NULL,
--   `nit` int(11) DEFAULT NULL,
--   `telefono` int(11) DEFAULT NULL,
--   `celular` int(11) DEFAULT NULL,
--   `direccion` varchar(400) DEFAULT NULL,
--   `notas` varchar(400) DEFAULT NULL,
--   `estado` tinyint(1) NOT NULL
-- );
-- ALTER TABLE `cliente`
--   ADD PRIMARY KEY (`id_cliente`);
-- ALTER TABLE `cliente`
--   MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT;



--
-- Estructura de tabla para la tabla `producto`
--
-- CREATE TABLE `producto` (
--   `id_producto` int(11) NOT NULL,
--   `nombre` varchar(150) NOT NULL,
--   `descripcion` varchar(400) DEFAULT NULL,
--   `estado` tinyint(1) NOT NULL
-- );
-- ALTER TABLE `producto`
--   ADD PRIMARY KEY (`id_producto`);
-- ALTER TABLE `producto`
--   MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT;



--
-- Estructura de tabla para la tabla `referencia`
--
-- CREATE TABLE `referencia` (
--   `id_referencia` int(11) NOT NULL,
--   `fecha` datetime NOT NULL,
--   `id_cliente` int(11) NOT NULL,
--   `id_producto` int(11) NOT NULL,
--   `id_usuario` int(11) NOT NULL,
--   `notas` varchar(400) DEFAULT NULL,
--   `estado` int(11) NOT NULL COMMENT '1=Activo, 2=Trabajado, 3=Inactivo',
--   `comentario_estado` varchar(400) DEFAULT NULL,
--   `id_usuario_estado` int(11) DEFAULT NULL
-- );
-- ALTER TABLE `referencia`
--   ADD PRIMARY KEY (`id_referencia`);
-- ALTER TABLE `referencia`
--   MODIFY `id_referencia` int(11) NOT NULL AUTO_INCREMENT;
-- OTRO NOMBRE A TABLA REFERIDOS
-- CREATE TABLE `asociado` (
--   `id_asociado` int(11) NOT NULL,
--   `fecha` datetime NOT NULL,
--   `id_cliente` int(11) NOT NULL,
--   `id_producto` int(11) NOT NULL,
--   `id_usuario` int(11) NOT NULL,
--   `notas` varchar(400) DEFAULT NULL,
--   `estado` int(11) NOT NULL COMMENT '1=Activo, 2=Inactivo',
--   `comentario_estado` varchar(400) DEFAULT NULL,
--   `id_usuario_estado` int(11) DEFAULT NULL
-- );
-- ALTER TABLE `asociado`
--   ADD PRIMARY KEY (`id_asociado`);
-- ALTER TABLE `asociado`
--   MODIFY `id_asociado` int(11) NOT NULL AUTO_INCREMENT;

--
-- Estructura de tabla para la tabla `bitacora`
--
-- CREATE TABLE `bitacora` (
--   `id_bitacora` int(11) NOT NULL,
--   `fecha` datetime NOT NULL,
--   `id_cliente` int(11) NOT NULL,
--   `id_usuario` int(11) NOT NULL,
--   `comentario` varchar(400)
-- );
-- ALTER TABLE `bitacora`
--   ADD PRIMARY KEY (`id_bitacora`);
-- ALTER TABLE `bitacora`
--   MODIFY `id_bitacora` int(11) NOT NULL AUTO_INCREMENT;