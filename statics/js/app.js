(function (Vue, axios, $) {
    console.log(window.location.href);
    new Vue({
        el: '#perfilCliente',
        data: {
            productos: [],
            referencias: [],
            bitacoras: [],
            dataReferencia: {
                producto: 0,
                notas: ''
            },
            comentarios: '',
            currentReferencia: 0,
            currentEstadoReferencia: 0,
            comentarioEstadoReferencia: '',
            currentBitacora: 0,
            idCliente: '',
            serverURL: '/api/clientes.php'
        },
        mounted: function () {
            this.idCliente = $('input[name=idClienteAJAX]').val();
            console.log(this.idCliente);
            if(this.idCliente>0){
                this.loadData();
            }
        },
        methods: {
            loadData: function () {
                this.loadProductos();
                this.loadRefencias();
                this.loadBitacoras();
            },
            loadProductos: function(){
                var _this = this;
                $.ajax({
                    type: "POST",
                    url: this.serverURL,
                    data: {idCliente: this.idCliente, act: 1},
                    success: function (resp) {
                        _this.productos = JSON.parse(resp);
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log("Porfavor reporta este error: " + errorThrown + xhr.status + xhr.responseText);
                    }
                });
            },
            loadRefencias: function(){
                var _this = this;
                $.ajax({
                    type: "POST",
                    url: this.serverURL,
                    data: {idCliente: this.idCliente, act: 2},
                    success: function (resp) {
                        _this.referencias = JSON.parse(resp);
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log("Porfavor reporta este error: " + errorThrown + xhr.status + xhr.responseText);
                    }
                });
            },
            loadBitacoras: function(){
                var _this = this;
                $.ajax({
                    type: "POST",
                    url: this.serverURL,
                    data: {idCliente: this.idCliente, act: 3},
                    success: function (resp) {
                        _this.bitacoras = JSON.parse(resp);
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log("Porfavor reporta este error: " + errorThrown + xhr.status + xhr.responseText);
                    }
                });
            },
            addReferencia: function() {
                if(this.dataReferencia.producto != 0){
                    var _this = this;
                    $.ajax({
                        type: "POST",
                        url: this.serverURL,
                        data: {act: 4, idCliente: this.idCliente, dataReferencia: this.dataReferencia},
                        success: function (resp) {
                            _this.referencias = JSON.parse(resp);
                            _this.dataReferencia.producto = 0;
                            _this.dataReferencia.notas = '';
                            var currentProducto = document.getElementById("selectProducto");
                            currentProducto.remove(currentProducto.selectedIndex);
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            console.log("Porfavor reporta este error: " + errorThrown + xhr.status + xhr.responseText);
                        }
                    });
                }
            },
            confirmRemoveReferencia: function(idReferencia){
                this.currentReferencia = idReferencia;
                $('#modalRemoveReferencia').modal('show');
            },
            removeReferencia: function(){
                // console.log(this.currentReferencia);
                if(this.currentReferencia != 0){
                    var _this = this;
                    $.ajax({
                        type: "POST",
                        url: this.serverURL,
                        data: {act: 5, idCliente: this.idCliente, idReferencia: this.currentReferencia},
                        success: function (resp) {
                            // console.log(resp);
                            $('#modalRemoveReferencia').modal('hide');
                            this.currentReferencia = 0;
                            _this.referencias = JSON.parse(resp);
                            _this.loadProductos();
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            console.log("Porfavor reporta este error: " + errorThrown + xhr.status + xhr.responseText);
                        }
                    });
                }
            },
            confirmCambioEstadoReferencia: function(idReferencia, estadoRefencia){
                this.currentReferencia = idReferencia;
                this.currentEstadoReferencia = estadoRefencia;
                $('#modalCambioEstadoReferencia').modal('show');
            },
            cambiarEstadoReferencia: function(){
                if(this.currentReferencia != 0 && this.currentEstadoReferencia != 0 && this.comentarioEstadoReferencia != ''){
                    var _this = this;
                    $.ajax({
                        type: "POST",
                        url: this.serverURL,
                        data: {act: 6, idCliente: this.idCliente, 
                            idReferencia: this.currentReferencia,
                            estadoReferencia: this.currentEstadoReferencia,
                            comentarioEstadoReferencia: this.comentarioEstadoReferencia},
                        success: function (resp) {
                            // console.log(resp);
                            $('#modalCambioEstadoReferencia').modal('hide');
                            _this.currentReferencia = 0;
                            _this.currentEstadoReferencia = 0;
                            _this.comentarioEstadoReferencia = '';
                            _this.referencias = JSON.parse(resp);
                            _this.loadProductos();
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            console.log("Porfavor reporta este error: " + errorThrown + xhr.status + xhr.responseText);
                        }
                    });
                }
            },
            addBitacora: function() {
                if(this.comentarios != ''){
                    var _this = this;
                    $.ajax({
                        type: "POST",
                        url: this.serverURL,
                        data: {act: 7, idCliente: this.idCliente, comentarios: this.comentarios},
                        success: function (resp) {
                            _this.bitacoras = JSON.parse(resp);
                            _this.comentarios = '';
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            console.log("Porfavor reporta este error: " + errorThrown + xhr.status + xhr.responseText);
                        }
                    });
                }
            },
            confirmRemoveBitacora: function(idBitacora){
                this.currentBitacora = idBitacora;
                $('#modalRemoveBitacora').modal('show');
            },
            removeBitacora: function(){
                // console.log(this.currentReferencia);
                if(this.currentBitacora != 0){
                    var _this = this;
                    $.ajax({
                        type: "POST",
                        url: this.serverURL,
                        data: {act: 8, idCliente: this.idCliente, idBitacora: this.currentBitacora},
                        success: function (resp) {
                            // console.log(resp);
                            $('#modalRemoveBitacora').modal('hide');
                            this.currentBitacora = 0;
                            _this.bitacoras = JSON.parse(resp);
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            console.log("Porfavor reporta este error: " + errorThrown + xhr.status + xhr.responseText);
                        }
                    });
                }
            },

            tabNewPedido: function () {
                // var serverURL = "http://localhost:8000/api/";
                var _this = this;
                axios.get(this.serverURL + "nuevoPedido").then(function (response) {
                    // console.log(response.data)
                    _this.pedidos = response.data;
                    // _this.pedidos.push();
                    setTimeout(() => {
                        $("#ident" + _this.pedidos[(response.data.length - 1)].numero).trigger("click");
                        _this.changeTabPedido(_this.pedidos[(response.data.length - 1)])
                    }, 90);
                });
            },
            changeTabPedido: function (pedidoCurrent) {
                // var serverURL = "http://localhost:8000/api/";
                var _this = this;
                _this.currentPedido = pedidoCurrent.pk;
                _this.currentSavePedido = pedidoCurrent
                _this.listProductsPedido = [];
                _this.tipoPedido = (pedidoCurrent.tipo) ? pedidoCurrent.tipo : '';
                _this.selectMesa = '';
                for (let index = 0; index < _this.mesas.length; index++) {
                    if (_this.mesas[index].pk == pedidoCurrent.mesa) {
                        _this.selectMesa = _this.mesas[index]
                    }
                }
                _this.datoCliente = {
                    nombre: (pedidoCurrent.nombre) ? pedidoCurrent.nombre : '',
                    telefono: (pedidoCurrent.telefono) ? pedidoCurrent.telefono : '',
                    direccion: (pedidoCurrent.direccion) ? pedidoCurrent.direccion : ''
                };

                axios.get(this.serverURL + "productsPedido/" + pedidoCurrent.pk).then(function (response) {
                    for (let i = 0; i < response.data.length; i++) {
                        _this.listProductsPedido.push(response.data[i]);
                    }
                });
            },
            changeCategory: function (categoria) {
                if (!categoria) {
                    this.selectedCategory = "all";
                } else {
                    this.selectedCategory = categoria.pk;
                }
            },
            addProductToPedido: function (addProducto) {
                var _this = this;
                // console.log(addProducto);
                addProducto['descuento'] = 0;
                addProducto['razon_descuento'] = "";
                addProducto['notas'] = "";
                addProducto['extra'] = "";
                addProducto['monto_extra'] = 0;
                if (_this.listProductsPedido.length > 0) {
                    var productoRepetido = false;
                    _this.listProductsPedido.forEach(function (producto) {
                        if (producto.pk == addProducto.pk || producto.producto_id == addProducto.pk) {
                            producto.cantidad++;
                            productoRepetido = true;
                            _this.listProductsPedido.push();
                        }
                    });
                    if (!productoRepetido) {
                        addProducto['cantidad'] = 1;
                        _this.listProductsPedido.push(addProducto);
                    }
                } else {
                    addProducto['cantidad'] = 1;
                    _this.listProductsPedido.push(addProducto);
                }
                // console.log(_this.listProductsPedido);
            },
            optionItem: function(idProducto) {
                var _this = this;
                this.currentItemOption = this.listProductsPedido[idProducto];
                $('#optionItem').modal('show').on('hide.bs.modal', function () {
                    _this.listProductsPedido.push();
                });
            },
            deleleItem: function (idProducto) {
                this.listProductsPedido.splice(idProducto, 1);
            },
            minusProduct: function (idProducto) {
                if (this.listProductsPedido[idProducto].cantidad > 1) {
                    this.listProductsPedido[idProducto].cantidad = this.listProductsPedido[idProducto].cantidad - 1;
                    this.listProductsPedido.push();
                }
            },
            incrementProduct: function (idProducto) {
                this.listProductsPedido[idProducto].cantidad = this.listProductsPedido[idProducto].cantidad + 1;
                this.listProductsPedido.push();
            },
            selectedTipoPedido: function (tipoPedido) {
                this.tipoPedido = tipoPedido;
                $('#tipoPedidoModal').modal('hide');
                // console.log(tipoPedido);
                if (this.tipoPedido == 1) {
                    $('#selectedMesaModal').modal({
                        backdrop: 'static',
                        keyboard: false
                    })
                    $('#selectedMesaModal').modal('show');
                } else {
                    $('#addClienteModal').modal('show');
                }
            },
            selectedMesa: function (mesa) {
                if (mesa.estado != 2) {
                    this.selectMesa = mesa;
                    $('#selectedMesaModal').modal('hide');
                }
            },
            savePedido: function () {
                _this = this;
                if (!this.tipoPedido) {
                    $('#tipoPedidoModal').modal('show');
                } else if (this.selectMesa == '' && this.tipoPedido == 1) {
                    $('#selectedMesaModal').modal('show');
                } else if (this.datoCliente.nombre == '' && this.tipoPedido != 1) {
                    $('#addClienteModal').modal('show');
                } else {
                    // var serverURL = "http://localhost:8000/api/pedido/guardar/";
                    $.ajax({
                        type: "POST",
                        url: this.serverURL + 'pedido/guardar/',
                        data: {
                            csrfmiddlewaretoken: this.token,
                            NoPedido: this.currentPedido,
                            listProducts: JSON.stringify(this.listProductsPedido),
                            tipoPedido: this.tipoPedido,
                            mesa: JSON.stringify(this.selectMesa),
                            datoCliente: JSON.stringify(this.datoCliente),
                            total: this.total
                        },
                        success: function (data) {
                            if (data.result) {
                                toastr.success('Pedido Guardado!');
                                // for (let index = 0; index < _this.pedidos.length; index++) {
                                // 	if (_this.pedidos[index].pk == _this.currentPedido) {
                                // 		_this.pedidos[index].estado = 2;
                                // 		_this.currentSavePedido = _this.pedidos[index];
                                // 	}
                                // }
                                _this.showPrint(_this.currentPedido);
                                _this.loadData();
                            } else {
                                toastr.error('Ocurrio un error al Guardar el Pedido');
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            console.log("Porfavor reporta este error: " + errorThrown + xhr.status + xhr.responseText);
                        }
                    });
                }
            },
            showPrint: function (idPedido) {
                axios.get(this.serverURL + "printPedido/" + idPedido).then(function (response) {
                    var pedido = (JSON.parse(response.data.pedido))[0]
                    var productos = JSON.parse(response.data.productos)

                    var tipoPedido = "";
                    var showClient
                    if (pedido.fields.tipo == 1) {
                        tipoPedido = "Mesa " + pedido.fields.mesa;
                        showClient = false
                    } else if (pedido.fields.tipo == 2) {
                        tipoPedido = "a Domicilio"
                        showClient = true
                    } else if (pedido.fields.tipo == 3) {
                        tipoPedido = "para Llevar"
                        showClient = true
                    }

                    tempPrint = `
						<table class="table table-bordered table-striped text-center" style="border-collapse: collapse;width: 100%;">
							<tbody>
								<tr style="border: 1px solid #ddd;">
									<td style="border: 1px solid #ddd;">Pedido: <br><b>#${ pedido.fields.numero }</b></td>
									<td style="border: 1px solid #ddd;">Fecha: <br><b>${ moment(pedido.fields.modificado).format("DD/MM/YYYY hh:mm:ss a") }</b></td>
									<td style="border: 1px solid #ddd;">Tipo: <br><b>${ tipoPedido }</b></td>
								</tr>
							</tbody>
						</table>
						<table class="table table-bordered table-striped text-center" style="border: 1px solid #ddd;background-color: #dddd; border-collapse: collapse;width: 100%;">
							<thead class='table-head'>
								<tr class='table-head'>
									<th style="border: 1px solid #ddd;">NOMBRE</th>
									<th style="border: 1px solid #ddd;">CANT.</th>
									<th style="border: 1px solid #ddd;">PRECIO</th>
									<th style="border: 1px solid #ddd;">TOTAL</th>
								</tr>
							</thead>
							<tbody>
								${ productos.map(producto => `
									<tr>
                                        <td style="border: 1px solid #ddd;">
                                        ${ producto.fields.monto_extra > 0 ? `<span style="float:right;font-size:14px;">(+Q${producto.fields.monto_extra})</span>` : `` }
                                        ${ producto.fields.descuento > 0 ? `<span style="float:right;font-size:14px;">(-${producto.fields.descuento}%)</span>` : `` }

                                        ${ producto.fields.nombre }
                                        ${ producto.fields.notas != '' ? `<sub>(Nota:${producto.fields.notas})</sub>` : `` }
                                        ${ producto.fields.extra != '' ? `<sub>(Extra: ${producto.fields.extra})</sub>` : `` }
                                        </td>
										<td style="border: 1px solid #ddd;">${ producto.fields.cantidad }</td>
										<td style="border: 1px solid #ddd;">Q.${ producto.fields.precio }</td>
										<td style="border: 1px solid #ddd;">Q.${ producto.fields.cantidad * producto.fields.precio }</td>
									</tr>
								`).join('')}
								<tr>
									<td colspan="3" style="border: 1px solid #ddd;">TOTAL</td>
									<td style="border: 1px solid #ddd;"><b>Q.${ pedido.fields.total }</b></td>
								</tr>
							</tbody>
						</table>
						${ showClient ? `
						<table class="table table-bordered table-striped text-center" style="border-collapse: collapse;width: 100%;">
							<tbody>
								<tr colspan="4" style="border: 1px solid #ddd;">
									<td colspan="4" style="border: 1px solid #ddd;" class="text-center"><b>Datos de Entrega<b></td>
								</tr>
								<tr style="border: 1px solid #ddd;">
									<td colspan="2" style="border: 1px solid #ddd;">Nombre:</td>
									<td colspan="2" style="border: 1px solid #ddd;">${ pedido.fields.nombre}</td>
								</tr>
								<tr>
									<td colspan="2" style="border: 1px solid #ddd;">Dirección:</td>
									<td colspan="2" style="border: 1px solid #ddd;">${ pedido.fields.direccion}</td>
								</tr>
								<tr>
									<td colspan="2" style="border: 1px solid #ddd;">Telefono:</td>
									<td colspan="2" style="border: 1px solid #ddd;">${ pedido.fields.telefono ? pedido.fields.telefono : `` }</td>
								</tr>
							</tbody>
						</table>`: ``}
					`;

                    $("#ticketToPrint").html(tempPrint);
                    $('#printPedido').modal('show');
                });

            },
            cobrarPedido: function () {
                _this = this;
                $.ajax({
                    type: "POST",
                    url: this.serverURL + 'pedido/cobrar/',
                    data: {
                        csrfmiddlewaretoken: this.token,
                        NoPedido: this.currentPedido,
                    },
                    success: function (data) {
                        if (data.result) {
                            toastr.success('Pedido Cobrado!');
                            // recargar pedidos y mesa
                            _this.loadData();

                        } else {
                            toastr.error('Ocurrio un error al Guardar el Pedido');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log("Porfavor reporta este error: " + errorThrown + xhr.status + xhr.responseText);
                    }
                });
            }
        },
        computed: {
            filteredProducts: function () {
                var _this = this;
                var listProducts;
                listProducts = _this.productos.filter(function (prod) {
                    return prod.nombre.toLowerCase().indexOf(_this.search.toLowerCase()) >= 0;
                });

                if (_this.selectedCategory == 'all') return listProducts;
                return listProducts.filter(function (productos) {
                    return productos.categoria == _this.selectedCategory;
                });
            },
            total: function () {
                return this.listProductsPedido.reduce(function (prev, product) {
                    return prev + ((product.cantidad * (product.precio - (product.precio * (product.descuento / 100)))) + parseInt(product.monto_extra));
                }, 0);
            }
        },
        updated: function () {
            // this.idCliente = $('input[name=idCliente]').val();
        }

    });
})(window.Vue, window.axios, window.$);