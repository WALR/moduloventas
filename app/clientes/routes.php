<?php 
$request_uri = explode('?', $_SERVER['REQUEST_URI'], 2);
$request_uri = trim( $request_uri[0], "/" );
$request_uri = explode( "/", $request_uri );

if(sizeOf($request_uri)>1){
    switch ($request_uri[1]) {
        case '':
            require 'app/clientes/busqueda.php';
            break;
        case 'lista':
            require 'app/clientes/lista.php';
            break;
        case 'nuevo':
            require 'app/clientes/nuevo.php';
            break;
        case 'editar':
            require 'app/clientes/editar.php';
            break;
        case 'perfil':
            require 'app/clientes/perfil.php';
            break;
        case 'productos':
            require 'app/clientes/productos.php';
            break;
        default:
            require 'app/clientes/busqueda.php';
            break;
    }
}else {
    require 'app/clientes/busqueda.php';
}

?>