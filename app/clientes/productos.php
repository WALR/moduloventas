<?php 
if(isset($_GET['id'])){
    require_once ("database/clientes.php");
    $clientesDB = new Clientes;
    $message = false;

    $result = $clientesDB->getID($_GET['id']);
    if($result->num_rows > 0){
        require_once ("database/productos.php");
        require_once ("database/subproductos.php");
        $productosDB = new Productos;
        $subproductosDB = new Subproductos;
        $productos = $productosDB->getAll();
        $subProductos = $subproductosDB->getAll();

        $clienteEdit = $result->fetch_assoc();
        if(isset($_POST['editarCliente'])) {
            if($clientesDB->agregarProductosCliente($_POST['editarCliente'], $clienteEdit['id_cliente'])){
                echo '<script type="text/javascript">window.location.href = "/clientes/perfil?id='.$clienteEdit['id_cliente'].'";</script>';
            }else {
                $message = true;
            }
        }

    }else{
        echo '<script type="text/javascript">window.location.href = "/clientes/lista";</script>';
    }
}else {
    echo '<script type="text/javascript">window.location.href = "/clientes/lista";</script>';
}
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Agregar Productos a Cliente</h3>
                </div>
                <form role="form" action="" method="post" name="formCliente">
                    <div class="box-body row">
                        <?php 
                            if($message){
                                echo '
                                <div class="pad margin">
                                    <div class="callout callout-danger" style="margin-bottom: 0!important;">
                                    A ocurrido un error, favor comunicate con el administrador del sistema.
                                    </div>
                                </div>
                                ';
                            }
                        ?>
                        <div class="form-group col-md-6">
                            <label>ID</label>
                            <input type="text" class="form-control" name="editarCliente[id_cliente]" value="<?php echo($clienteEdit['id_cliente']) ?>" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Codigo Asociado</label>
                            <input type="text" class="form-control" name="editarCliente[codigo_asociado]" value="<?php echo($clienteEdit['codigo_asociado']) ?>" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Primer Nombre</label>
                            <input type="text" class="form-control" name="editarCliente[primer_nombre]" placeholder="Primer Nombre" value="<?php echo($clienteEdit['primer_nombre']) ?>" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Segundo Nombre</label>
                            <input type="text" class="form-control" name="editarCliente[segundo_nombre]" placeholder="Segundo Nombre" value="<?php echo($clienteEdit['segundo_nombre']) ?>" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Primer Apellido</label>
                            <input type="text" class="form-control" name="editarCliente[primer_apellido]" placeholder="Primer Apellido" value="<?php echo($clienteEdit['primer_apellido']) ?>" disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Segundo Apellido</label>
                            <input type="text" class="form-control" name="editarCliente[segundo_apellido]" placeholder="Segundo Apellido" value="<?php echo($clienteEdit['segundo_apellido']) ?>" disabled>
                        </div>
                        <h5 class="col-md-12"><i class="fa fa-angle-double-right text-primary"></i> Productos Interesado</h5>
                        <div class="form-group col-md-6">
                            <label>Fecha de Probable de Adquidir</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" name="editarCliente[fecha_probable]">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <?php 
                                foreach ($productos as $producto) {
                                    $subpro = "";
                                    foreach ($subProductos as $subProducto) {
                                        // echo $subProducto['nombre'];
                                       if($subProducto['id_producto']==$producto['id_producto']){
                                           $subpro .= '
                                            <tr>
                                                <td>
                                                    <label>
                                                        <input type="checkbox" class="" name="editarCliente[productos][]" value="'.$subProducto['id_subproducto'].'"> '.$subProducto['nombre'].'
                                                    </label>
                                                </td>
                                            </tr>
                                           ';
                                        }
                                    }
                                    
                                    echo '
                                    <table class=" table-bordered table-striped col-md-4">
                                        <thead>
                                            <tr>
                                                <th>'.$producto['nombre'].'</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            '.$subpro.'
                                        </tbody>
                                    </table>
                                    ';
                                }
                            ?>

                        </div>
                        <div class="form-group col-md-12">
                            <label>Notas</label>
                            <textarea rows="3" class="form-control" name="editarCliente[notas]" placeholder="Notas"></textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="/clientes/lista" class="btn btn-default">Cancel</a>
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</a></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>