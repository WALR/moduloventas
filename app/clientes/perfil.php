<?php 
if(isset($_GET['id'])){
    require_once ("database/users.php");
    $usuariosDB = new Users;
    require_once ("database/clientes.php");
    $clientesDB = new Clientes;
    $result = $clientesDB->getID($_GET['id']);
    if($result->num_rows > 0){
        $clientePerfil = $result->fetch_assoc();
        // [ADD] Obtener la lista de referidos del usuario
        $showInteres = true;
        $resultInteres = $clientesDB->getInteresCliente($_GET['id']);
        if($resultInteres->num_rows > 0){
            $interes = $resultInteres->fetch_assoc();
            $InteresProductos = $clientesDB->getInteresProductos($interes['id_interes_cliente']); 

            $resultUsuario = $usuariosDB->getID($interes['id_usuario']);
            $userData = $resultUsuario->fetch_assoc();

            $showInteres = true;

        }else{
            $showInteres = false;
        }
        
    }else{
        echo '<script type="text/javascript">window.location.href = "/clientes/lista";</script>';
    }
}else {
    echo '<script type="text/javascript">window.location.href = "/clientes/lista";</script>';
}
?>
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="/statics/img/cliente.png" alt="Imagen Cliente">
                        <?php 

                            if($clientePerfil["fecha_nacimiento"]!='0000-00-00'){
                                $fnacimiento ='<td>'.DateTime::createFromFormat('Y-m-d',$clientePerfil["fecha_nacimiento"])->format('d/m/Y').'</td>';

                            }else{
                                $fnacimiento = "";
                            }

                            echo '
                            <h5 class="profile-username text-center">'.$clientePerfil['primer_nombre'].' '.$clientePerfil['segundo_nombre'].' '.$clientePerfil['primer_apellido'].' '.$clientePerfil['segundo_apellido'].'</h5>
                            <p class="text-muted text-center">'.(($clientePerfil["estado"])? "<span class='label label-info'>Activo</span>": "<span class='label label-default'>Inactivo</span>").'</p>
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Codigo Asociado</b> <a class="pull-right">'.(($clientePerfil['codigo_asociado'])? $clientePerfil['codigo_asociado']: "No Asociado").'</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Fecha Nacimiento</b> <a class="pull-right">'.$fnacimiento.'</a>
                                </li>
                                <li class="list-group-item">
                                    <b>DPI</b> <a class="pull-right">'.$clientePerfil['dpi'].'</a>
                                </li>
                            </ul>
                            <a href="/clientes/editar?id='.$clientePerfil['id_cliente'].'" class="btn btn-primary btn-block"><b>Editar</b></a>
                            ';
                        
                        ?>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Información de Contacto</h3>
                </div>
                <div class="box-body">
                    <strong><i class="fa fa-phone"></i> Telefono/Celular</strong>
                    <p class="text-muted">
                        <?php echo 'Telefono: '.$clientePerfil['telefono'].'<br> Celular: '.$clientePerfil['celular']; ?>
                    </p>
                    <hr>
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Direccion</strong>
                        <p class="text-muted"><?php echo $clientePerfil['direccion']; ?></p>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Productos de Interes</h3>
                    <div class="pull-right btn-control">
                        <a href="/clientes/productos?id=<?php echo($clientePerfil['id_cliente']); ?>" class="btn btn-link">
                            <i class="fa fa-edit"></i>
                            Editar
                        </a>
                    </div>
                </div>
                <?php 
                if($showInteres){
                ?>
                <div class="box-body">
                    <dl>
                        <div class="col-md-6">
                            <dt>Fecha Registrado:</dt>
                            <dd><?php echo(DateTime::createFromFormat('Y-m-d H:i:s',$interes["fecha"])->format('d/m/Y H:i:s')); ?></dd>
                        </div>
                        <div class="col-md-6">
                            <dt>Fecha a adquirir:</dt>
                            <dd><?php echo(DateTime::createFromFormat('Y-m-d H:i:s',$interes["fecha_adquirir"])->format('d/m/Y')); ?></dd>
                        </div>
                        <div class="col-md-6">
                            <dt>Notas:</dt>
                            <dd><?php echo($interes["notas"]); ?></dd>
                        </div>
                        <div class="col-md-6">
                            <dt>Usuario Quien registro:</dt>
                            <?php 
                                echo '
                                <dd><a href="/usuarios/perfil?id='.$userData["id_usuario"].'">'.$userData["username"].'</a></dd>
                                ';
                            ?>
                            
                        </div>
                    </dl>

                    <h5 class="col-md-12"><i class="fa fa-angle-double-right text-primary"></i> Productos de Interes</h5>
                    <ul class="list-group list-group-unbordered col-md-12" style="
    text-align: center;">
                        <?php 
                        foreach ($InteresProductos as $producto) {
                           echo '
                            <li class="list-group-item">
                                <b>'.$producto['nombre'].'</b>
                            </li>
                           ';
                        }
                        ?>
                    </ul>
                </div>
                <?php 
                }else {
                ?>
                <div class="box-body">
                    <dl>
                        <div class="col-md-12">
                            No tiene ningun producto de interes registrado. <a href="/clientes/productos?id=<?php echo($clientePerfil['id_cliente']); ?>" class="btn btn-primary btn-block"><b>Registrar Productos de Interes</b></a>
                        </div>
                    </dl>

                </div>
                <?php 
                }
                ?>
            </div>
        </div>
    </div>
</section>