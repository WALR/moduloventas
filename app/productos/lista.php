<?php 
require_once ("database/productos.php");
$productosDB = new Productos;
$productos = $productosDB->getAll();

?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Productos</h3>
                    <div class="pull-right btn-control">
                        <a href="/productos/nuevo/" class="btn btn-block btn-primary">
                            <i class="fa fa-plus-square"></i>
                            Agregar Producto
                        </a>
                    </div>

                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped tableData">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach($productos as $producto) {
                                $estado = ($producto["estado"])? "<span class='label label-info'>Activo</span>": "<span class='label label-default'>Inactivo</span>";
                                echo '
                                    <tr>
                                        <td>'.$producto["id_producto"].'</td>
                                        <td>'.$producto["nombre"].'</td>
                                        <td>'.$producto["descripcion"].'</td>
                                        <td>'.$estado.'</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="/productos/editar?id='.$producto["id_producto"].'" title="Editar" class="btn bg-navy btn-flat" data-toggle="tooltip"><i class="fa fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    ';
                            }
                            
                            ?>
                    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>