<?php 
require_once ("database/users.php");
$usuariosDB = new Users;
$message = false;
if(isset($_POST['username'], $_POST['password'])) {
    // echo ($usuariosDB->login($_POST['username'], $_POST['password']));
    if($usuariosDB->login($_POST['username'], $_POST['password'])){
        // header("Location: /");
        exit(header("Location: /"));
    }else {
        $message = true;
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login | Cooperativa Guayacán</title>
    <link rel="shortcut icon" href="/statics/img/guayacan.png">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="../../statics/dist/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../statics/dist/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../statics/dist/bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="../../statics/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="../../statics/dist/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        body{
            background: url('/statics/img/fondo2.png') no-repeat center center fixed !important;
            /* -webkit-background-size: 100% auto !important;
            -moz-background-size: 100% auto !important;
            -o-background-size: 100% auto !important; */
            background-size: 100% auto !important;
            height: 0;
            /* -webkit-filter: blur(1px); */
            /*margin-top: 12%;    */
        }
        @media only screen and (max-width: 915px) {
            body {
                background: #d2d6de !important;
            }
        }
    </style>

</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-box-body">
        <div class="login-logo">
            <img src="../../statics/img/guayacan.png" height="90" alt="Logo Guayacan">
        </div>
        <p class="login-box-msg">Inicia Sesión</p>
        
        <?php 
            if($message){
                echo '
                <div class="alert alert-danger alert-error">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <h5>No se ha podido iniciar sesión. Verifica tu usuario y/o contraseña.</h5>
                </div>';
            }
        ?>
        <form action="" method="post">
            <div class="form-group has-feedback">
                <input type="text" name="username" class="form-control" placeholder="Nombre de Usuario" required>
                <i class="fa fa-user form-control-feedback"></i>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Contraseña" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="../../statics/dist/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../../statics/dist/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../statics/dist/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
