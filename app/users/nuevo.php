<?php 
require_once ("database/users.php");
$usuariosDB = new Users;
$message = false;
if(isset($_POST['nuevoUsuario']['nombre'], $_POST['nuevoUsuario']['apellido'], $_POST['nuevoUsuario']['usuario'], $_POST['nuevoUsuario']['password'])) {
    if($usuariosDB->create($_POST['nuevoUsuario'])){
        echo '<script type="text/javascript">window.location.href = "/usuarios";</script>';
    }else {
        $message = true;
    }
}
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Nuevo Usuarios</h3>
                </div>
                <form role="form" action="" method="post" name="formUsuario">
                    <div class="box-body row">
                        <div class="form-group col-md-6">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" name="nuevoUsuario[nombre]" placeholder="Nombre" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="apellido">Apellido</label>
                            <input type="text" class="form-control" name="nuevoUsuario[apellido]" placeholder="Apellido" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="usuario">Usuario</label>
                            <input type="text" class="form-control" name="nuevoUsuario[usuario]" placeholder="Usuario" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password">Contraseña</label>
                            <input type="password" class="form-control" name="nuevoUsuario[password]" placeholder="Contraseña" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="numero_empleado">Numero Empleado</label>
                            <input type="number" class="form-control" name="nuevoUsuario[numero_empleado]" placeholder="Numero Empleado">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Rol</label>
                            <select class="form-control" name="nuevoUsuario[rol]">
                                <option value="0">Usuario</option>
                                <option value="1">Administrador</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Estado</label>
                            <select class="form-control" name="nuevoUsuario[estado]">
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="/usuarios/" class="btn btn-default">Cancel</a>
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</a></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>