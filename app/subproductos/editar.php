<?php 
if(isset($_GET['id'])){
    require_once ("database/productos.php");
    $productosDB = new Productos;
    $productos = $productosDB->getAll();
    require_once ("database/subproductos.php");
    $subproductosDB = new Subproductos;
    $result = $subproductosDB->getID($_GET['id']);
    if($result->num_rows > 0){
        $subproductoEdit = $result->fetch_assoc();

        if(isset($_POST['editarSubproducto']['nombre'], $_POST['editarSubproducto']['estado'])) {
            if($subproductosDB->edit($subproductoEdit['id_subproducto'], $_POST['editarSubproducto'])){
                echo '<script type="text/javascript">window.location.href = "/subproductos";</script>';
            }else {
                $message = true;
            }
        }

    }else{
        echo '<script type="text/javascript">window.location.href = "/subproductos";</script>';
    }
}else {
    echo '<script type="text/javascript">window.location.href = "/subproductos";</script>';
}
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Editar Sub Producto</h3>
                </div>
                <form role="form" action="" method="post" name="formUsuario">
                    <div class="box-body row">
                        <div class="form-group col-md-6">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" name="editarSubproducto[nombre]" placeholder="Nombre" value="<?php echo($subproductoEdit['nombre']); ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Estado</label>
                            <select class="form-control" name="editarSubproducto[estado]">
                                <option value="1" <?php echo(($subproductoEdit['estado'])? 'selected' : ''); ?>>Activo</option>
                                <option value="0" <?php echo(($subproductoEdit['estado'])? '': 'selected'); ?>>Inactivo</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Productos</label>
                            <select class="form-control" name="editarSubproducto[producto]">
                                <?php 

                                foreach ($productos as $producto) {
                                    $select = (($producto['id_producto']==$subproductoEdit['id_producto'])? 'selected': '');

                                    echo '<option value="'.$producto['id_producto'].'" '.$select.'>'.$producto['nombre'].'</option>';
                                }
                                
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="descripcion">Descripcion</label>
                            <textarea rows="3"  class="form-control" name="editaSubpProducto[descripcion]" placeholder="Descripcion"><?php echo($subproductoEdit['descripcion']); ?></textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="/subproductos/" class="btn btn-default">Cancel</a>
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</a></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>