<?php 
require_once ("database/subproductos.php");
$subproductosDB = new Subproductos;
$subproductos = $subproductosDB->getAll();

?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Sub Productos</h3>
                    <div class="pull-right btn-control">
                        <a href="/subproductos/nuevo/" class="btn btn-block btn-primary">
                            <i class="fa fa-plus-square"></i>
                            Agregar Subproducto
                        </a>
                    </div>

                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped tableData">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Producto</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach($subproductos as $subproducto) {
                                $estado = ($subproducto["estado"])? "<span class='label label-info'>Activo</span>": "<span class='label label-default'>Inactivo</span>";
                                echo '
                                    <tr>
                                        <td>'.$subproducto["id_subproducto"].'</td>
                                        <td>'.$subproducto["nombre"].'</td>
                                        <td>'.$subproducto["descripcion"].'</td>
                                        <td>'.$subproducto["nombre_producto"].'</td>
                                        <td>'.$estado.'</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="/subproductos/editar?id='.$subproducto["id_subproducto"].'" title="Editar" class="btn bg-navy btn-flat" data-toggle="tooltip"><i class="fa fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    ';
                            }
                            
                            ?>
                    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>