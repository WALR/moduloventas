<?php 
require_once ("conection.php");
date_default_timezone_set('America/Guatemala');

class Clientes{
	private $conection;
    private $nameTable;

    public function __construct(){
        $this->conection = new Conection;
        $this->nameTable = "cliente";
    }

    public function getAll(){
        $db = $this->conection->initConection();
        $query = "SELECT * FROM {$this->nameTable}";
        return $this->conection->runquery($db, $query);
    }

    public function getID($id){
        $db = $this->conection->initConection();
        $query = "SELECT * FROM ".$this->nameTable." WHERE id_cliente = ".$id.";";
        return $this->conection->runquery($db, $query);
    }

    public function getInteresCliente($id){
        $db = $this->conection->initConection();
        $query = "SELECT * FROM interes_cliente WHERE id_cliente = ".$id.";";
        return $this->conection->runquery($db, $query);
    }

    public function getInteresProductos($idInteres){
        $db = $this->conection->initConection();
        $query = "SELECT ic.interes_cliente, ic.id_subproducto, s.nombre  FROM interes_detalle ic 
                LEFT JOIN subproducto s ON s.id_subproducto = ic.id_subproducto
                WHERE ic.interes_cliente = ".$idInteres.";";
        return $this->conection->runquery($db, $query);
    }


    public function create($data){
        if(isset($data['codigo_asociado'])){
            $result = $this->checkClienteExiste($data);
            if($result->num_rows > 0){
                $cliente = $result->fetch_assoc();
                return array('code'=> 2, 'data'=> $cliente);
            }else{
                return $this->guardarCliente($data);
            }
        }else{
            return $this->guardarCliente($data);
        }
    }

    public function guardarCliente($data){
        $db = $this->conection->initConection();
        $Fnacimiento = ((empty($data['fecha_nacimiento']))?'null':DateTime::createFromFormat('d/m/Y', $data['fecha_nacimiento'])->format('Y-m-d'));
        
        
        $query = "INSERT INTO ".$this->nameTable." 
                (codigo_asociado, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, 
                fecha_nacimiento, dpi, telefono, celular, 
                direccion, estado) 
                VALUES 
                (".((empty($data['codigo_asociado']))?'null':$data['codigo_asociado']).", '".$data['primer_nombre']."', '".$data['segundo_nombre']."', '".$data['primer_apellido']."', '".$data['segundo_apellido']."', 
                '".$Fnacimiento."', ".((empty($data['dpi']))?'null':$data['dpi']).", ".((empty($data['telefono']))?'null':$data['telefono']).", ".((empty($data['celular']))?'null':$data['celular']).",
                '".$data['direccion']."', 1);";
        if($this->conection->runquery($db, $query)){
            $result = $this->conection->runquery($db, "SELECT LAST_INSERT_ID()");
            $idCliente = $result->fetch_assoc();
            $clienteResponse = array('id'=>$idCliente['LAST_INSERT_ID()'], 'primer_nombre'=>$data['primer_nombre'], 'primer_apellido'=>$data['primer_apellido']);
            if($data['productos']){
                if($this->agregarInteres($data, $idCliente['LAST_INSERT_ID()'])){
                    return array('code'=> 1, 'data'=> $clienteResponse);
                }else{
                    return array('code'=> 3, 'data'=> $clienteResponse);
                }
            }
            return array('code'=> 1, 'data'=> $clienteResponse);
        }else {
            return array('code'=> 4, 'data'=> '');
        }
    }

    public function agregarProductosCliente($data, $idCliente){
        $db = $this->conection->initConection();
        $query = "DELETE FROM interes_cliente WHERE id_cliente = ".$idCliente.";";
        if($this->conection->runquery($db, $query)){
            return $this->agregarInteres($data, $idCliente);
        }
    }

    public function agregarInteres($data, $idCliente){
        $hoy = date("Y-m-d H:i:s");
        $fechaAdquidir = DateTime::createFromFormat('d/m/Y', $data['fecha_probable'])->format('Y-m-d');

        $db = $this->conection->initConection();
        $query = "INSERT INTO interes_cliente (fecha, fecha_adquirir, id_cliente, id_usuario, notas, estado) 
        VALUES ('".$hoy."', '".$fechaAdquidir."', ".$idCliente.", ".$_SESSION['id'].", '".$data['notas']."', 1);";
        // return $this->conection->runquery($db, $query);
        if($this->conection->runquery($db, $query)){
            $result = $this->conection->runquery($db, "SELECT LAST_INSERT_ID()");
            $idInteres = $result->fetch_assoc();

            foreach ($data['productos'] as $idProducto) {
                $this->agregarInteresDetalle($idInteres['LAST_INSERT_ID()'], $idProducto); 
            }

            return true;

        }
    }

    public function agregarInteresDetalle($idInteres, $idProducto){
        $db = $this->conection->initConection();
        $query = "INSERT INTO interes_detalle (interes_cliente, id_subproducto) VALUES (".$idInteres.", ".$idProducto.");";
        return $this->conection->runquery($db, $query);
    }

    public function edit($id, $data){
        $db = $this->conection->initConection();
        $Fnacimiento = DateTime::createFromFormat('d/m/Y', $data['fecha_nacimiento'])->format('Y-m-d');
        $estado = ((isset($data['estado']))? ",estado = ".$data['estado']: "");
        $query = "UPDATE ".$this->nameTable." SET
                primer_nombre = '".$data['primer_nombre']."', segundo_nombre = '".$data['segundo_nombre']."', primer_apellido = '".$data['primer_apellido']."',
                segundo_apellido = '".$data['segundo_apellido']."', fecha_nacimiento = '".$Fnacimiento."', dpi = ".((empty($data['dpi']))?'null':$data['dpi']).",
                telefono = ".((empty($data['telefono']))?'null':$data['telefono']).", celular = ".((empty($data['celular']))?'null':$data['celular']).", direccion = '".$data['direccion']."' 
                ".$estado." WHERE id_cliente = ".$id.";";
        return $this->conection->runquery($db, $query);
    }

    public function buscarCliente($data){
        $db = $this->conection->initConection();
        $Snombre = ((empty($data['segundo_nombre']))? "": "OR segundo_nombre LIKE '".$data['segundo_nombre']."' ");
        $Sapellido = ((empty($data['segundo_apellido']))? "": "OR segundo_apellido LIKE '".$data['segundo_apellido']."' ");
        $dpi = ((empty($data['dpi']))? "": "AND dpi = ".$data['dpi']);
        if(!empty($data['fecha_nacimiento'])){
            $FechaFormat = DateTime::createFromFormat('d/m/Y', $data['fecha_nacimiento'])->format('Y-m-d');
            $Fnacimiento = "AND fecha_nacimiento ='".$FechaFormat."'";
        }else {
            $Fnacimiento = "";
        }
        $query = "SELECT * FROM ".$this->nameTable." WHERE primer_nombre LIKE '%".$data['primer_nombre']."%' OR primer_apellido LIKE '%".$data['primer_apellido']."%' 
                ".$Snombre." ".$Sapellido." ".$dpi." ".$Fnacimiento.";";
        if(isset($data['codigo_asociado'])){
            $query = "SELECT * FROM ".$this->nameTable." WHERE  codigo_asociado= ".$data['codigo_asociado'].";";
        }
        
        return $this->conection->runquery($db, $query);
    }

    public function delete(){

    }

    public function agregarReferencia($idCliente, $idProducto, $notas, $idUsuario){
        $hoy = date("Y-m-d H:i:s");
        $db = $this->conection->initConection();
        $query = "INSERT INTO referencia (fecha, id_cliente, id_producto, id_usuario, notas, estado) 
                VALUES ('".$hoy."', ".$idCliente.", ".$idProducto.", ".$idUsuario.", '".$notas."', 1);";
        return $this->conection->runquery($db, $query);
    }


    public function checkClienteExiste($data){
        // $Fnacimiento = DateTime::createFromFormat('d/m/Y', $data['fecha_nacimiento'])->format('Y-m-d');
        $db = $this->conection->initConection();
        $query = "SELECT id_cliente, codigo_asociado, primer_nombre, primer_apellido, fecha_nacimiento FROM ".$this->nameTable." 
                WHERE codigo_asociado = ".$data['codigo_asociado']."";
        // return $this->conection->runquery($db, $query);
        // $result =  
        return $this->conection->runquery($db, $query);
        
    }

    public function getProductosNoReferidos($idCliente){
        $db = $this->conection->initConection();
        $query = "SELECT id_producto, nombre FROM producto WHERE estado = 1 
                AND id_producto NOT IN 
                (SELECT id_producto FROM referencia WHERE estado = 1 AND id_cliente = ".$idCliente.")";
        return $this->conection->runquery($db, $query);
    }

    public function getClienteReferencias($idCliente){
        $db = $this->conection->initConection();
        $query ="SELECT r.id_referencia, r.fecha, r.notas, r.estado, r.comentario_estado, p.nombre as nombre_producto, 
                u.nombre as nombre_usuario, u.id_usuario as id_usuario, id_usuario_estado, u2.nombre as nombre_estado
                FROM referencia r INNER JOIN producto p ON p.id_producto = r.id_producto 
                INNER JOIN usuario u ON u.id_usuario = r.id_usuario
                LEFT JOIN usuario u2 ON u2.id_usuario = r.id_usuario_estado
                WHERE id_cliente =".$idCliente." ORDER BY r.fecha DESC;";
        return $this->conection->runquery($db, $query);
    }

    public function getClienteBitacoras($idCliente){
        $db = $this->conection->initConection();
        $query ="SELECT b.id_bitacora, b.fecha, b.comentario, b.id_usuario, u.nombre as nombre_usuario 
                FROM bitacora b INNER JOIN usuario u ON u.id_usuario = b.id_usuario 
                WHERE id_cliente =".$idCliente." ORDER BY b.fecha DESC;";
        return $this->conection->runquery($db, $query);
    }

    public function deleteReferencia($idReferencia){
        $db = $this->conection->initConection();
        $query ="DELETE FROM referencia WHERE id_referencia =".$idReferencia.";";
        return $this->conection->runquery($db, $query);
    }

    public function cambiarEstadoReferencia($idReferencia, $estado, $comentarioEstado, $idUsuario){
        $db = $this->conection->initConection();
        $query ="UPDATE referencia SET estado =".$estado.", comentario_estado = '".$comentarioEstado."',
                id_usuario_estado = ".$idUsuario." WHERE id_referencia =".$idReferencia.";";
        return $this->conection->runquery($db, $query);
    }

    public function agregarBitacora($idCliente, $comentario, $idUsuario){
        $hoy = date("Y-m-d H:i:s");
        $db = $this->conection->initConection();
        $query = "INSERT INTO bitacora (fecha, id_cliente, id_usuario, comentario) 
                VALUES ('".$hoy."', ".$idCliente.", ".$idUsuario.", '".$comentario."');";
        return $this->conection->runquery($db, $query);
    }

    public function deleteBitacora($idBitacora){
        $db = $this->conection->initConection();
        $query ="DELETE FROM bitacora WHERE id_bitacora =".$idBitacora.";";
        return $this->conection->runquery($db, $query);
    }
    
}


?>