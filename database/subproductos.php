<?php 
require_once ("conection.php");
class Subproductos{
	private $conection;
    private $nameTable;

    public function __construct(){
        $this->conection = new Conection;
        $this->nameTable = "subproducto";
    }

    public function getAll(){
        $db = $this->conection->initConection();
        $query = "SELECT s.id_subproducto, s.nombre, s.descripcion, s.estado, s.id_producto, p.nombre as nombre_producto FROM {$this->nameTable} s INNER JOIN producto p ON p.id_producto = s.id_producto";
        return $this->conection->runquery($db, $query);
    }

    public function getID($id){
        $db = $this->conection->initConection();
        $query = "SELECT id_subproducto, nombre, descripcion, estado, id_producto FROM ".$this->nameTable." WHERE id_subproducto = ".$id.";";
        return $this->conection->runquery($db, $query);
    }

    public function create($data){
        $db = $this->conection->initConection();
        $query = "INSERT INTO ".$this->nameTable." (nombre, descripcion, estado, id_producto) 
                VALUES ('".$data['nombre']."', '".$data['descripcion']."', ".$data['estado'].", ".$data['producto'].");";
        return $this->conection->runquery($db, $query);
    }

    public function edit($id, $data){
        $db = $this->conection->initConection();
        $query = "UPDATE ".$this->nameTable." SET nombre = '".$data['nombre']."', descripcion = '".$data['descripcion']."', estado = ".$data['estado'].", id_producto = ".$data['producto']." WHERE id_subproducto = ".$id.";";
        return $this->conection->runquery($db, $query);
    }

    public function delete($id){
        $this->conection->initConection();
        $query ="DELETE FROM ".$this->nameTable." WHERE id_subproducto =".$id.";";
        return $this->conection->runquery($query);
    }
}


?>