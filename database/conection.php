<?php
class Conection{

    private $host;
    private $user;
    private $pass;
    private $database;
    
    public function __construct() {
        // $this->host = "localhost";
        $this->host = "192.168.64.2";
        $this->user = "ModuloVentasDBUser";
        $this->pass = "ModuloVentasDBpass**";
        $this->database = "ModuloVentasDB";
    }

    function initConection() {
        $db = new mysqli($this->host, $this->user, $this->pass, $this->database);
        $db->query("SET NAMES 'UTF8' ");

        if($db->connect_errno > 0){
            die('Unable to connect to database [' . $db->connect_error . ']');
        }
        
        return $db;
    }

    function runQuery($db, $sql) {
        if(!$resultado = $db->query($sql)){
            die('Error al ejecutar el Query [' . $db->error . ']');
        }
        return $resultado;
    }

    function closeConection($db) {
        $db->close();
    }

}

?>