<?php 
require_once ("conection.php");
class Users{
	private $conection;
    private $nameTable;

    public function __construct(){
        $this->conection = new Conection;
        $this->nameTable = "usuario";
    }

    public function getAll(){
        $db = $this->conection->initConection();
        $query = "SELECT u.id_usuario, u.username, u.nombre, u.apellido, u.numero_empleado, r.nombre as nombre_rol, estado FROM {$this->nameTable} u INNER JOIN rol r ON r.id_rol = u.rol";
        return $this->conection->runquery($db, $query);
    }

    public function getID($id){
        $db = $this->conection->initConection();

        $query = "SELECT u.id_usuario, u.username, u.nombre, u.apellido, u.numero_empleado, r.nombre as nombre_rol, estado FROM {$this->nameTable} u INNER JOIN rol r ON r.id_rol = u.rol WHERE u.id_usuario = ".$id.";";
        return $this->conection->runquery($db, $query);
    }

    public function create($data){
        $db = $this->conection->initConection();

        $query = "INSERT INTO ".$this->nameTable." (username, password, nombre, apellido, numero_empleado, rol, estado) 
                VALUES ('".$data['usuario']."', '".password_hash($data['password'], PASSWORD_DEFAULT)."', '".$data['nombre']."', 
                '".$data['apellido']."', ".$data['numero_empleado'].", ".$data['rol'].", ".$data['estado'].");";
    
        return $this->conection->runquery($db, $query);
    }

    public function edit($id, $data){
        $db = $this->conection->initConection();

        $pass = (!empty($data['password']))? ",password =  '".password_hash($data['password'], PASSWORD_DEFAULT)."'": "";
        $query = "UPDATE ".$this->nameTable." SET username = '".$data['usuario']."', nombre = '".$data['nombre']."', 
                apellido = '".$data['apellido']."', numero_empleado = ".$data['numero_empleado'].", rol = ".$data['rol'].", 
                estado = ".$data['estado']." ".$pass."  WHERE id_usuario = ".$id.";";

        return $this->conection->runquery($db, $query);
    }
    
    public function editPassword($id, $actualPass, $nuevoPass){
        $db = $this->conection->initConection();

        $result = $this->getID($id);
        $user = $result->fetch_assoc();
        if(password_verify($actualPass, $user['password'])){
            $query = "UPDATE ".$this->nameTable." SET password = '".password_hash($nuevoPass, PASSWORD_DEFAULT)."' WHERE id_usuario = ".$id.";";
            return $this->conection->runquery($db, $query);
        } else {
            return false;
        }
    }

    public function delete($id){
        $db = $this->conection->initConection();
        $query ="DELETE FROM ".$this->nameTable." WHERE id_usuario =".$id.";";
        return $this->conection->runquery($db, $query);
    }

    public function login($user, $pass){
        $db = $this->conection->initConection();
        $query = "SELECT id_usuario, username, password, nombre, apellido, numero_empleado, rol, estado FROM ".$this->nameTable." 
                WHERE username = '".$user."';";
        $result =  $this->conection->runquery($db, $query);
        if($result->num_rows > 0) {
            $user = $result->fetch_assoc();
            if(password_verify($pass, $user['password'])){
                $_SESSION['login'] = True;
                $_SESSION['id'] = $user['id_usuario'];
                $_SESSION['username'] = $user['username'];
                $_SESSION['nombre'] = $user['nombre'];
                $_SESSION['apellido'] = $user['apellido'];
                $_SESSION['numero_empleado'] = $user['numero_empleado'];
                $_SESSION['rol'] = $user['rol'];
                
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }

    public function getReferidosUsuario($idUsuario){
        $db = $this->conection->initConection();

        $query ="SELECT c.id_cliente, c.primer_nombre, c.segundo_nombre, c.primer_apellido, c.segundo_apellido,
                c.fecha_nacimiento, c.dpi, c.telefono, c.celular, c.estado 
                FROM interes_cliente r INNER JOIN cliente c ON c.id_cliente = r.id_cliente 
                WHERE id_usuario =".$idUsuario;
                // GROUP BY c.id_cliente;";
        // echo $query;   
        return $this->conection->runquery($db, $query);
    }


    public function getAllRol(){
        $db = $this->conection->initConection();
        $query = "SELECT id_rol, codigo, nombre FROM rol;";
        return $this->conection->runquery($db, $query);
    }
}


?>